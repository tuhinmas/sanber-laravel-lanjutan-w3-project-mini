<?php

namespace App\Http\Controllers\Api;

use App\Models\Todo;
use Illuminate\Http\Request;
use App\Http\Requests\TodoRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\TodoResource;
use Illuminate\Support\Facades\Auth;


class TodoController extends Controller
{
  public function index(){
      $id = Auth::id();
      $todos = Todo::where('user_id' , $id)->orderBy('created_at' , 'desc')->get();

      return TodoResource::collection($todos);
    }

    public function store(TodoRequest $request){
      $user = Auth::id();
      // dd($user);
      $todo = new Todo;
      // $todo = auth()->user()->create([
      //   'text' => $request->todo,
      //   'done' => 0,
      //   // 'user_id' => $user,
      // ]);

      $todo->text = $request->todo;
      $todo->done = 0;
      $todo->user_id = $user;
      $todo->save();
      // dd($todo);
      // event(new TodoCreatedEvent($todo));

      return new TodoResource($todo);
    }

    public function delete($id){
      Todo::destroy($id);
      return 'success';
    }

    public function changeDoneStatus($id){

      $todo = Todo::find($id);
      if($todo->done == 1){
        $update = 0;
      }else{
        $update = 1;
      }

      $todo->update([
        'done' => $update
      ]);

      return new TodoResource($todo);
    }
}
