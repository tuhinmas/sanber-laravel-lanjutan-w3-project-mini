<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TodoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
     public function toArray($request)
     {
      //  $text = "mas";
         return [
           'id' => $this->id,
           'text' => $this->text,
           'done' => $this->done,
           'user_id' => $this->user_id,
         ];
     }
}
